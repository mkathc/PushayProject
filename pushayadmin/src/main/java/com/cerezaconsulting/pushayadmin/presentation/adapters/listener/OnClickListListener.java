package com.cerezaconsulting.pushayadmin.presentation.adapters.listener;

/**
 * Created by miguel on 15/03/17.
 */

public interface OnClickListListener {
    void onClick(int position);
}
